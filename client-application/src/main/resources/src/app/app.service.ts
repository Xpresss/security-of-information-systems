import {Injectable} from '@angular/core';
import {Cookie} from 'ng2-cookies';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

export class Foo {
  constructor(
    public id: number,
    public name: string) {
  }
}

@Injectable()
export class AppService {
  public clientId = 'Web Client Application';
  public redirectUri = 'https://localhost:8089/';

  private userDetailsUrl = 'https://localhost:8444/resource-server/user';

  constructor(
    private _http: HttpClient) {
  }

  retrieveToken(code) {
    let params = new URLSearchParams();
    params.append('grant_type', 'authorization_code');
    params.append('client_id', this.clientId);
    params.append('redirect_uri', this.redirectUri);
    params.append('code', code);

    let headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Basic ' + btoa(this.clientId + ":secret")
    });
    this._http.post('https://localhost:8443/authorization-server/oauth/token', params.toString(), {headers: headers})
      .subscribe(
        data => this.saveToken(data),
        err => alert('Invalid Credentials')
      );
  }

  // Refresh Token
  refreshToken() {
    let params = new URLSearchParams();
    params.append('grant_type', 'refresh_token');
    params.append('refresh_token', Cookie.get('refresh_token'));

    let headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Basic ' + btoa(this.clientId + ":secret")
    });
    this._http.post('https://localhost:8443/authorization-server/oauth/token', params.toString(), {headers: headers})
      .subscribe(
        data => this.saveToken(data),
        err => alert('Invalid Credentials')
      );

  }

  saveToken(token) {
    var expireDate = new Date().getTime() + (1000 * token.expires_in);
    Cookie.set("access_token", token.access_token, expireDate);
    Cookie.set('refresh_token', token.refresh_token, expireDate);
    console.log('Obtained Access token');
    window.location.href = 'https://localhost:8089';
  }

  getResource(resourceUrl): Observable<any> {
    var headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer ' + Cookie.get('access_token')
    });
    return this._http.get(resourceUrl, {headers: headers})
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  checkCredentials() {
    return Cookie.check('access_token');
  }

  logout() {
    // this._http.get('https://localhost:8443/authorization-server/logout').subscribe(noop);

    Cookie.delete('access_token');
    Cookie.delete('refresh_token');
    // window.location.reload();

    window.location.href = 'https://localhost:8443/authorization-server/logout';
  }

  getUserDetails(): Observable<any> {
    const headers = new HttpHeaders({
      'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
      'Authorization': 'Bearer ' + Cookie.get('access_token')
    });

    return this._http.get(this.userDetailsUrl, {headers: headers});
  }
}
