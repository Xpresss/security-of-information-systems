export interface User {
  name: string;
  role: string;
}

export function generateUser(name: string, role: string) {
  return {
    name: name,
    role: role
  }
}
