import {Component} from '@angular/core';
import {AppService} from './app.service'
import {generateUser, User} from "./user.model";

@Component({
  selector: 'home-header',
  providers: [AppService],
  template: `
    <div class="container">
      <button *ngIf="!isLoggedIn" class="btn btn-primary" (click)="login()" type="submit">Login</button>
      <div *ngIf="isLoggedIn" class="content">
        <span><b>Welcome </b><span class="text-primary">{{user?.name}} [{{user?.role}}]</span></span>
        <a class="btn btn-default pull-right" (click)="logout()" href="#">Logout</a>
        <br/>
        <foo-details></foo-details>
      </div>
    </div>`
})

export class HomeComponent {
  public isLoggedIn = false;

  public user: User;


  constructor(
    private _service: AppService) {
  }

  ngOnInit() {
    this.isLoggedIn = this._service.checkCredentials();
    let i = window.location.href.indexOf('code');
    if (!this.isLoggedIn && i != -1) {
      this._service.retrieveToken(window.location.href.substring(i + 5));
    }

    if (this.isLoggedIn) {
      this._service.getUserDetails().subscribe(data => this.user = generateUser(data.name, data.role));
    }
  }

  login() {
    window.location.href = 'https://localhost:8443/authorization-server/oauth/authorize?response_type=code&client_id=' + this._service.clientId + '&redirect_uri=' + this._service.redirectUri;
  }

  logout() {
    this._service.logout();
  }
}
