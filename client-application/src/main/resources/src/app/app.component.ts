import {Component} from '@angular/core';
 
@Component({
    selector: 'app-root',
    template: `<header>
      <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container">
          <div id="navbar" class="collapse navbar-collapse">
            <a class="navbar-brand" href="#">Client Application</a>
          </div>
        </div>
      </nav>
    </header>
<router-outlet></router-outlet>`
})

export class AppComponent {
}
