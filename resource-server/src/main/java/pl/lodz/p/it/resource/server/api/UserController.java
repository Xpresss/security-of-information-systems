package pl.lodz.p.it.resource.server.api;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.Map;

@Controller
public class UserController {

//    @PreAuthorize("#oauth2.hasScope('read')")
    @PreAuthorize("#oauth2.hasScope('Account_details')")
    @RequestMapping(method = RequestMethod.GET, value = "/users/extra")
    @ResponseBody
    public Map<String, Object> getExtraInfo(Authentication auth) {
        OAuth2AuthenticationDetails oauthDetails = (OAuth2AuthenticationDetails) auth.getDetails();
        Map<String, Object> details = (Map<String, Object>) oauthDetails.getDecodedDetails();
        System.out.println("User organization is " + details.get("organization"));
        return details;
    }

    @PreAuthorize("#oauth2.hasScope('Account_details')")
    @RequestMapping(value = "/user", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> user(Authentication authentication, Principal principal) {

        Map<String, String> map = new LinkedHashMap<>();
        map.put("name", principal.getName());
        map.put("role", authentication.getAuthorities().iterator().next().getAuthority());
        return map;
    }
}
