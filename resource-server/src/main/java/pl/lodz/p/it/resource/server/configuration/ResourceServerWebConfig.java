package pl.lodz.p.it.resource.server.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@ComponentScan({"pl.lodz.p.it.resource.server.api"})
public class ResourceServerWebConfig implements WebMvcConfigurer {
    //
}
