package pl.lodz.p.it.api;

import org.springframework.security.oauth2.provider.AuthorizationRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

@Controller
@SessionAttributes("authorizationRequest")
public class ViewsController {

    @RequestMapping("/login")
    public String loginPage() {
        return "login";
    }

    @RequestMapping("oauth/confirm_access")
    public ModelAndView authorizePage(Map<String, Object> model) {
        AuthorizationRequest clientAuth = (AuthorizationRequest) model.remove("authorizationRequest");

        model.put("client", clientAuth.getClientId());
        model.put("scope", clientAuth.getScope());

        return new ModelAndView("authorize", model);
    }
}
