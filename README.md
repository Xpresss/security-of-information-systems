## Security of Information Systems OAuth 2.0

### Authors:
* Dawid Dziedziczak
* Michał Ladra
* Maciej Zakrzewski

## Build the Project
```
mvn clean install
```

## Projects/Modules
This project contains 3 modules : 
- `authorization-server` - the Authorization Server (port = 8443)
- `resource-server` - the Resource Server (port = 8444)
- `client-application` - Authorization Code Grant UI Module - using Angular 6 (port = 8089)


## Run the Modules
You can run any spring-based sub-module using command line: 
```
mvn spring-boot:run
```

## Run the Angular 6 Module

- To run `client-application` - Angular6 frontend module, we need to build the app first:
```
mvn clean install
```

- Then we need to navigate to our Angular app directory:
```
cd src/main/resources
```


- Finally, we will start our app:
```
npm start
```